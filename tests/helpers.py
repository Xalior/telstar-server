import json
import telstar
import os

k_dev_settings_file = "telstar-dev.json"

class TestHelper:

    def setUp(self):

        try:
            base_path = os.path.abspath("../")
            settings_file = k_dev_settings_file
            path_to_config_file = "{0}/{1}".format(base_path, settings_file)
            settings = telstar.Config(path_to_config_file).settings

            self.auth = telstar.Auth(settings["dbcon"])
            self.dal = telstar.Dal(settings["dbcon"])

            # insert some test users
            self.auth.insert_user(100, "password100")
            self.auth.insert_user(101, "password101")

            # insert some test pages to secondary
            self.dal.insert_document(self.create_frame_edit_tf(100, "a"), False)
            self.dal.insert_document(self.create_frame_edit_tf(101, "a"), False)
            self.dal.insert_document(self.create_frame_edit_tf(1001, "a"), False)
            self.dal.insert_document(self.create_frame_edit_tf(10, "a"), False)
            pass

        except Exception as ex:
            print(ex)

    def tearDown(self):
        try:
            # remove test frames
            self.dal.delete_frame(100, "a")
            self.dal.delete_frame(101, "a")
            self.dal.delete_frame(1001, "a")
            self.dal.delete_frame(10, "a")
            #
            # # remove test users
            self.auth.delete_user(100)
            self.auth.delete_user(101)
            pass

        except Exception as ex:
            print(ex)

    def frame_exists(self, page_no: int, frame_id: str):

        if self.dal.get_document(page_no, frame_id):
            return True
        else:
            return False

    @staticmethod
    def create_frame_empty(page_no: int , frame_id: str, json: bool = False):

        frame = {
            "pid": {
                "page-no": page_no,
                "frame-id": frame_id
            },
            "visible": True
        }

        if json:
            return json.dumps(frame)
        else:
            return frame

    @staticmethod
    def create_frame_edit_tf(page_no: int , frame_id: str, json: bool = False):

        frame = {
            "pid": {
                "page-no": page_no,
                "frame-id": frame_id
            },
            "visible": True,
            "content": {
                "data": "https://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBiwY4UCBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBBChQ0EFAgMIECBAghQoaCEgQIECBAgQIECBAgQIECBAgQIECBAgQIECAwgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAXaoECBAg1IDCBAgQNUCBAgQakCBAgQIECBAgQIECBAgQIEBdqgQIECDUgMIECBA1QIECBBqQIECBAgQIECBAgQIECBAgQF2qBAgQINSAwgQIEDVAgQIEGpAgQIECBAgQIECBAgQIECBAXaoECBAg1IDCBAgQN0aNGjR60CBAgQIECBAgQIECBAgQIEBdqgQIECDUgMIECBA1QIIKBBqQIECBAgQIECBAgQIECBAgQF-vDhw4cPSAwgQIEHXhw4cOHpAgQIECBAgQIECBAgQIECBAXaoECBAg1IDCBAgQNUCBAgQakCBAgQIECBAgQIECBAgQIEBdqgQQUCDUgMIECBA1QIISBBqQIECBAgQIECBAgQIECBAgQF-vDhw4cPSAwgQIEHXhw4cOHpAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
                "data": "http://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQICn_OmTJkSJApUqVKlSpUlcHApiTJkyZEiRIkyZMmTIkSJFmKf3JX58____RBg-fPnzog__2iBBo-fNX9qgQcOHDh8-fGiAp_zFf______2v7_____7X___oEGr__Qa0ur_________QoCn9ywK__6D__a__7dH__tf___1QIv_9AiQIkaPX_bo0aMpiKf_-Yr__oP_9r__tUH_-1____9og__yjnz5cldX9qUc-fPkp__uSv_-g__2v_-1Qf_7X__aIv____KZv_dIV__2pTN___yn__mK___z__a____5__tf_9qgVf__8o5_vWJX__alHP___Kf_7kr____6dBr_____-h__2qBBr__ymbPnSFf69CUzf__8pnz5iq9ejRoECBWvXr0KBGjQYOHBHz4cECBAgwcCily7__yqDhw4ePnzogQeP7RAg0fPiBB___9X___QYPH___-sCmb__KoP________3B__tUCDV__tUH___1f36XB-_______KOf_8qgRo__9B__v9X___QINX___Qav__B8-NNX_-jR___8pm__ymYrq______h_odX9qg1f___7q__9X_-h1f_5TBgwYM37__KOSur-_Ro0Pr_8_f_qDV_b___r__1f_6D___lFKlSpUu__8pmK_v7VAgwf______tNX9r______V__oP__-gQaPnwpm__yqDB__tUCD__QIEG___1f2qL____9X_-g____z5-__ymb__KoNX_-lQKl6VAgQav6_V__oNf___1f2qBX_____9-hKOf_8qgVJ0JRw4cOPLkqiQoESNGgVL0aFEjQoECJGjRoymDNm__ymTJkzZv3_____-3bt27du3Thw8efPnz58-fPnz58-fP___QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
                "type": "edit.tf"
            }
        }
        if json:
            return json.dumps(frame)
        else:
            return frame

    @staticmethod
    def create_frame_markup(page_no: int , frame_id: str, json: bool = False):
        frame = {
            "pid": {
                "page-no": page_no,
                "frame-id": frame_id
            },
            "visible": True,
            "title": {
                "data": "[D]Welcome\r\n\r\n",
                "type": "markup"
            },
            "content": {
                "data": "[Y]Hello[C]Telstar[M]World",
                "type": "markup"
            }
        }
        if json:
            return json.dumps(frame)
        else:
            return frame

    @staticmethod
    def create_frame_paras(page_no: int , frame_id: str, json: bool = False):

        content = ["[Y]Hello",
                   "[C]Telstar",
                   "[M]World"]

        frame = {
            "pid": {
                "page-no": page_no,
                "frame-id": frame_id
            },
            "visible": True,
            "title": {
                "data": "[D]Welcome\r\n\r\n",
                "type": "markup"
            },
            "content": {
                "data": content,
                "type": "markup"
            }
        }
        if json:
            return json.dumps(frame)
        else:
            return frame

    @staticmethod
    def create_frame_raw(page_no: int , frame_id: str, json: bool = False):
        frame = {
            "pid": {
                "page-no": page_no,
                "frame-id": frame_id
            },
            "visible": True,
            "title": {
                "data": "\x1b\x4dWelcome\r\n\r\n",
                # "type": "raw"
            },
            "content": {
                "data": "\x1b\x43Hello\x1b\x46Telstar\x1b\x45World",
                # "type": "raw"
            }
        }
        if json:
            return json.dumps(frame)
        else:
            return frame
