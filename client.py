import sys
import os

from enum import Enum
from tkinter import *
from socket import *
from telstar.log_utils import *

logger = getlogger(__name__)

# ORIGINAL TORNADO EXAMPLES
# https://stackoverflow.com/questions/40239901/tornado-tcp-server-client-process-communication/40257248#40257248
# https://github.com/tornadoweb/tornado/blob/master/demos/tcpecho/client.py

# OTHER TORNADO EXAMPLES
# https://gist.github.com/metachris/7305650
# http://www.tornadoweb.org/en/stable/iostream.html

# nx.nxtel.org 23280

# TODO: Add Telnet Parser, probably shouldn't remove telnet negotiation as modems may need it.

from socket import socket

#server_host = 'nx.nxtel.org'
#server_port = 23280

#server_host = '167.99.205.178'
server_host = 'glasstty.com'
server_port = 6502
#server_host = 'fish.ccl4.org'
#server_port = 23


class CharacterAttribute:

    class Colour(Enum):
        alpha_red = 0x41
        alpha_green = 0x42
        alpha_yellow = 0x43
        alpha_blue = 0x44
        alpha_magenta = 0x45
        alpha_cyan = 0x46
        alpha_white = 0x47
        mosaic_red = 0x51
        mosaic_green = 0x52
        mosaic_yellow = 0x53
        mosaic_blue = 0x54
        mosaic_magenta = 0x55
        mosaic_cyan = 0x56
        mosaic_white = 0x57

    class Background(Enum):
        black_background = 0x5c
        new_background = 0x5d

    def __init__(self):
        self.colour_attribute = self.CharAttrColour.alpha_white
        self.flashing = False
        self.double_height = False
        self.conceal_display = False
        self.contiguous_graphics = True
        self.background = self.CharAttrBackground.black_background


class ViewdataClient(Frame):

    def __init__(self, client, parent=None):

        self.client = client
        self.main_window = parent


        Frame.__init__(self, parent)
        self.pack(expand=YES, fill=BOTH)
        self.text = None
        self.make_widgets()

        self.text.focus_set()
        self.text.bind("<Key>", self.key)


        self.winfo_toplevel().title("TELSTAR Terminal")
        self.main_window.mainloop()

    def make_widgets(self):

        keypad = Frame(self.main_window)
        keypad.pack(side=RIGHT, expand=YES)

        row1 = Frame(keypad)
        row1.pack(side=TOP, expand=YES,fill=X)
        row2 = Frame(keypad)
        row2.pack(side=TOP, expand=YES,fill=X)
        row3 = Frame(keypad)
        row3.pack(side=TOP, expand=YES,fill=X)
        row4 = Frame(keypad)
        row4.pack(side=TOP, expand=YES,fill=X)

        b1 = Button(row1)
        b1.config(text='1', width="1", command=(lambda: self.send_char(b'1')))
        b1.pack(side=LEFT)

        b2 = Button(row1)
        b2.config(text='2', width="1", command=(lambda: self.send_char(b'2')))
        b2.pack(side=LEFT)

        b3 = Button(row1)
        b3.config(text='3', width="1", command=(lambda: self.send_char(b'3')))
        b3.pack(side=LEFT)

        b4 = Button(row2)
        b4.config(text='4', width="1", command=(lambda: self.send_char(b'4')))
        b4.pack(side=LEFT)

        b5 = Button(row2)
        b5.config(text='5', width="1", command=(lambda: self.send_char(b'5')))
        b5.pack(side=LEFT)

        b6 = Button(row2)
        b6.config(text='6', width="1", command=(lambda: self.send_char(b'6')))
        b6.pack(side=LEFT)

        b7 = Button(row3)
        b7.config(text='7', width="1", command=(lambda: self.send_char(b'7')))
        b7.pack(side=LEFT)

        b8 = Button(row3)
        b8.config(text='8', width="1", command=(lambda: self.send_char(b'8')))
        b8.pack(side=LEFT)

        b9 = Button(row3)
        b9.config(text='9', width="1", command=(lambda: self.send_char(b'9')))
        b9.pack(side=LEFT)

        bstar = Button(row4)
        bstar.config(text='*', width="1", command=(lambda: self.send_char(b'*')))
        bstar.pack(side=LEFT)

        b0 = Button(row4)
        b0.config(text='0', width="1", command=(lambda: self.send_char(b'0')))
        b0.pack(side=LEFT)

        bhash = Button(row4)
        bhash.config(text='#', width="1", command=(lambda: self.send_char(b'\x5f')))
        bhash.pack(side=LEFT)

        text = Text(self.main_window)
        text.config(font = ('bedstead extended', 16, 'normal'))
        text.config(width = 40, height = 24)
        text.pack(side = LEFT, expand = YES)
        self.text = text

    def set_text(self, text = ''):

        self.text.delete('1.0', END)
        self.text.insert('1.0', text)
        self.text.mark_set(INSERT, '1.0')
        self.text.focus()

    def get_text(self):

        return self.text.get('1.0', END+'-1c')

    def send_char(self, char):

        raw_data = self.client.send_request(char)

        if raw_data is not None and len(raw_data) > 0:
            vdata = self.process_data(raw_data)
            #TODO swap this for a character at a time process, see below
            self.set_text(vdata)

        # print('[client] vdata {0}/{1} ({2})'.format(hex(ord(u)), u, ord(u)))

        else:
            print('[client] no data received.')

    def process_data(self, data):

        char_count = 0
        viewdata = u''

        escape = False
        graphics_mode = False

        for c in data:

            # debug nonesense
            if c == 13 or c== 10:
                cs = ' '
            else:
                cs = chr(c)

            print('{0} [{1}], {2} (Char Count: {3})'.format(hex(c), c, cs, char_count))
            # end of debug nonesense

            # checking for escaped charater
            if escape:

                if(c >= 0x51 and c <= 0x57):
                    graphics_mode = True
                elif (c >= 0x41 and c <= 0x47):
                    graphics_mode = False

                if c >= 0x40 and c <= 0x5f:
                    # rest the escape flag
                    escape = False
                    # TODO this will trigger a change in colour, background etc.
                    # set control char to space
                    c = 0x20

            elif graphics_mode:
                if c >= 0x20 and c <=0x3f:
                    c += 0xEE00 - 0x20
                if c >= 0x60 and c <=0x7f:
                    c +=  0xEE40 - 0x60

            # TODO Sort control chars
            if c >= 0x20 or c in [0x0a, 0x1b]:

                viewdata += chr(c)

                # don't count LFs etc
                if c == 0x0a:
                    char_count = 0

                    # reset defaults
                    graphics_mode = False
                    escape = False

                elif c == 0x1b:
                    escape = True
                else:
                    char_count += 1

            # keep track of the columns, cols that are 40 chars will not have a trailing LF
            # and we need one for the display
            if char_count >= 40:
                viewdata += '\n'
                char_count = 0

                # reset defaults
                graphics_mode = False
                escape = False

        # return a list of attributed characters so that they cam be displayed in whatever form.
        return viewdata

    def key(self, event):
        print
        "pressed", repr(event.char)

    def quit(self):
        self.stream.close()
        sys.exit()


class TCPClient:

    def __init__(self, address, port ):

        try:
            self.socket_obj = socket(AF_INET, SOCK_STREAM)
            self.socket_obj.settimeout(0.01) # crude but effective non-blocking approach
            self.socket_obj.connect((address, port))

        except Exception as ex:
            print("[client] connection error {0}".format(type(ex)))

    def send_request (self, message):

        self.socket_obj.send(message)
        data = b''
        byts = None
        try:
            byts = self.socket_obj.recv(1152)  # 1024 + 128 just to be safe
            while len(byts) > 0:
                data += byts
                byts = self.socket_obj.recv(1152)  # 1024 + 128 just to be safe
        except timeout as ex:
            print("[client] receive socket timeout")
        except Exception as ex:
            print("[client] send request {0}".format(type(ex)))

        return data

    def close(self):
        self.socket_obj.close()
        print('Bye!')


if __name__ == '__main__':

    # create the view window, attach it to the root window and pass in the client


    # create a root window and add the app window
    root = Tk()
    # root.geometry('640x512')

    client = TCPClient(server_host, server_port)

    st = ViewdataClient(client, root)
    st.main_window.mainloop()






















