docker build -f Dockerfile.amd64 --rm --no-cache --tag johnnewcombe/telstar:latest --tag johnnewcombe/telstar:amd64-0.29 .
docker build -f Dockerfile.arm64v8 --rm --no-cache  --tag johnnewcombe/telstar:arm64v8-0.29 .
docker push johnnewcombe/telstar:amd64-0.29
docker push johnnewcombe/telstar:arm64v8-0.29
docker push johnnewcombe/telstar:latest

