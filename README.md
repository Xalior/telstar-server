# The Telstar Videotex System

![Pages_wide.png](https://bitbucket.org/repo/LXaBk6A/images/987672554-Pages_wide.png)

TELSTAR is both a a videotex *application* and a videotex *service*. Details of the current videotex service can be found at [https://glasstty.com](https://glasstty.com). The information and code here relate to the TELSTAR *application*. Further details of using and implementing this application can be found in the [TELSTAR Wiki](https://bitbucket.org/johnnewcombe/telstar-server/wiki/Home).

## Introduction

The TELSTAR videotex system, is a modern application designed to run within a  Docker environment provides a simple viewdata/videotext platform similar to those that were prevalent during the 1980s such as Prestel [https://en.wikipedia.org/wiki/Prestel](https://en.wikipedia.org/wiki/Prestel). The aim of the system is to provide a viewdata/videotex experience for anyone who is interested in how things 'used to be'. Services in the past typically provided access via the public switched telephone network (PSTN), and whilst this is also true for TELSTAR the service, the TELSTAR application detailed here makes use of a simple internet connection for modern internet modems.

The implementation described here is a full implementation of TELSTAR application, and whilst the system is comprehensive, is fairly simple to set up. The system can be extended using a very simple plugin architecture and this can be used to update content (frames) as well as perform other periodic tasks as required.

Content for the TELSTAR system is based on simple json files with support for the popular frame editor available at [https://edittf](https://edit.tf). Frames can also be created programatically with Python.

The TELSTAR videotex application is typically accessed over the internet using modern *internet modems* in conjunction with historic Viewdata and videotex terminals and home computers of the 1980s (see below). There are also some client applications that can be used with modern computers and mobile devices.

The TELSTAR application can be installed into a Docker environment (see [https://docker.com](https://docker.com) with a few simple Docker commands, Docker Compose or Kubernetes. In addition, an example system can be installed and set up with a simple Docker command.

For full details of the system and its use see the [Telstar Wiki](https://bitbucket.org/johnnewcombe/telstar-server/wiki/Home).

## Getting Started

Telstar is implemented using Docker containers and, in addition to the Docker commands detailed below, TELSTAR can be managed using Portainer and examples are available for orchestrating the services using both Docker Compose and Kubernetes. For full details see the [Telstar Wiki](https://bitbucket.org/johnnewcombe/telstar-server/wiki/Home).

To run Telstar using Docker, a minimum of two Docker images are required.

* The official Docker image **mongodb**
* The Docker image **telstar**

In addition, a Docker *network* will need to created to allow the images to communicate, and a Docker *volume* is desirable to allow for configuration changes and the addition of plugins.

### Create the Docker Network and Volumes

The Docker network is used to allow communication between the TELSTAR application and the *mongo* database. The volumes provide a means to persist configuration data, any added plugins and the *mongo* database itself.

The following commands will create the resources required.

    $ docker network create telstar-network
    $ docker volume create telstar-volume
    $ docker volume create mongo-volume

### Pull the Images and Run the Containers

Pull the required images as follows.

    $ docker pull johnnewcombe/telstar
    $ docker pull mongo

*Please note that the tag used with the official mongo image may need to be set to the system architecture that is to be used. Please see https://hub.docker.com/_/mongo.*

There are several ways to run the TELSTAR application, this *Getting Started* example uses simple Docker commands.

Start the *mongodb* container and connect it to the previously created network using the command below. 

*Please note that the '--name' parameter is important as it is used as part of the database connection string from the TELSTAR container.*

    $ docker run --name telstar-mongo --network telstar-network -v mongo-volume:/data/db -d -e MONGO_INITDB_ROOT_USERNAME=mongoadmin -e MONGO_INITDB_ROOT_PASSWORD=secret mongo

The Docker *run* command can be used to start the Telstar container and connect it to the previously created network. Note that in the example below the internal port is specified as 6512 and that this is mapped to the same port on the host system. This is also specified at the end of the command to inform TELSTAR that this is the port to listen on.

    $ docker run --rm -d --name telstar-server --network telstar-network -p 6512:6512 -v telstar-volume:/opt/telstar/volume johnnewcombe/telstar server --port=6512

In order to add some example content to the system, the *install* parameter can be used as in the example below.

    $ docker run --rm --name telstar-server-install --network telstar-network  johnnewcombe/telstar install-example

If the *--install* switch is used as in the example above, the service should be running with example content and can be accessed using a suitable client.

The simplest way to test the system from a desktop machine is to use Telnet. TELSTAR is not a Telnet application but telnet can be used to provide a *sanity check* e.g.

    $ telnet <ip-of-docker-host> <port-used-for-telstar>

If the system is working, a display similar to the following would be shown.

    Connected to glasstty.com.
    Escape character is '^]'.
	
	
    20201128T1316Z
    L             TE
     ST
    Welcome     
    
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
             Welcome to theTELSTAR
               videotex service.
	
         You are connected toELIOT.
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    ...	

The screen looks a little garbled simply because TELSTAR is a videotex application not a Telnet application.

As previously mentioned the Docker environmemnt can be managed using Portainer, and an example of creating a simple SSH tunnel to a remote server for portainer is described in the [Telstar Wiki](https://bitbucket.org/johnnewcombe/telstar-server/wiki/Home).

![portainer.png](https://bitbucket.org/repo/LXaBk6A/images/2827664245-portainer.png)

## Pages and Frames

As is typical with interactive videotex systems, pages are numbered from 0 to 999999999 and consist of one or more frames with the suffix a-z, for example the first frame for page 200 would be 200a, the second 200b and so on (see [Routing](routing.md) for more details). These frames are stored within TELSTARs database as *JSON* objects (see [https://www.json.org/](https://www.json.org/)).

The simplest frame that can be viewed on TELSTAR would be defined as follows. This would create a simple information frame with some default content.

    {
      "pid": {
        "page-no": 101,
        "frame-id": "a"
      },
      "visible": true,
    }

Whilst frames can be created directly with *JSON*, it is often simpler to use the *frame_document* object as described in the example shown in the next section.

Shown below is a more comple example of a JSON defined frame. With this particular frame, the actual viewdata content has been created using the editor at [http://edit.tf](http://edit.tf), this can be seen in the *content.data* and *content.type* keys. The example below is a simple information page with some simple routing (see [Routing](routing.md)). Pasting the *content.data* value into a browser will show the page and allow it to be edited. 

    {
      "pid": {
        "page-no": 0,
        "frame-id": "a"
      },
      "visible": true,
      "frame-type": "information",
      "content": {
        "data": "http://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQMMKAbNw6dyCTuyZfCBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgKJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRAxQR4s6LSgzEEmdUi0otOogkzo0-lNg1JM-cCg8unNYgQIASBBj67OnXllWIMu7pl5dMOndty7uixBoy4dnTQsQIECBAgBIEHfLh2dNCDDuyINmnNl59POzKuQIECBAgQIECBAgQIECBAyQTotemggzoiCvFg1JEWkCnYemnfuw7EGHdkQIECBAgQIASBBp3dMvLdh6ad-7DsQbsvfmgw7siDvlw9NGXkuQIECBAgQM0EKrTkzotOmgkzo0-lNg1JM-cCh79vDDu8oMO7IgQIECAEgQYuvPTuy8-aDdl781wM6EjVKcVBMw9MvPogoctOPLzQIEDRBHn1otKdNizqiCTOjT6U2DUkz5wKlvw5OaxByw6dixAgBIEGHdkQUMPLZpw7cu7ouQIECBAgQIECBAgQIECBAgQIECBA2QR4NSLXg2UFOLSrSYcWmCnWKkWYsQVIsWNBsLEEOHMaIASBBh3ZEHTRlQQ9-zfz54diCHh7ZUGHJ2y7unXllXIECBAgQN0EifMkxINmmCqcsPbLsQYd2RBI37NOTD5QbsvfmuQIECBA4CHQc2TDpT50WogcMGCAScgoOnLTi69MqDpvQdNGnmgQIAaBBzy8u2nHlQd9PTQgqZdmXnvzdO-HllQYd2RBt38sq5AgQOQ0yfHQT40ZYgp2adSLNQSZ0aegTIKkWnUQUIMeLTQIECAokSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEFeLBqSItJBGn0osODTqIBIM6EQIEFDDnyoFTJywvoECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
        "type": "edit.tf"
      },
      "routing-table": [0,1,11,3,400,5,6,7,800,9,0],
      "cursor": false,
      "author-id": "editf-frame-generator",
      "navmsg-forecolour": "yellow",
      "navmsg-backcolour": "blue",
      "navmsg-highlight": "white"
    }

TELSTAR supports many different types of frame, including *response frames* that can capture user data and *gateway frames* that provide connectivity to other systems. 

It is not necessary to use the [http://edit.tf](http://edit.tf) editor, this is just one option. Full details of all of the frame types and how to create them is described in the section [Frames](frames.md).

### Adding Frames to the System

There are two main methods by which frames can be added to the system.

* Plugins
* Telstar API 

#### Telstar Plugins

Plugins are most suitable for adding and updating multiple pages at the same time. Plugins are simple to create and deploy and can be run during initialisation of the system or as required using the base operating system scheduler e.g. *cron*. This makes plugins idea for initial setup of the system and updates to dynamic pages periodically.

__Further details of the Telstar Plugin framework and how to create them can be found [Here](telstar_plugins.md).__

#### Telstar API

Whilst using a plugin is ideal to create a set of initial pages and also for updating any dynamically changing pages e.g. news feeds etc. It may on occaision be more appropriate to use the Telstar API to make updates.

The TELSTAR API can be used to retrieve and update TELSTAR frames for the purpose of updating viewdata content and configuring routing etc.

The API is a restful API that uses the GET, PUT, POST and DELETE HTTP methods. This makes it a simple process to add new frames and update existing frames.

Data is passed to and from the API in JSON format, making the system very simple to use in any programming language. The whole API could be used without any programming at all by using a command line utility such as CURL. 

For full details see the [Telstar Wiki](https://bitbucket.org/johnnewcombe/telstar-server/wiki/Home).
