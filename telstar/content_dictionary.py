import math
import re

from collections import namedtuple
from enum import Enum
from bs4 import BeautifulSoup

from telstar.exceptions import *
from telstar.globals import *
from telstar.log_utils import *

logger = getlogger(__name__)

# create a named tuple class called ContentLine with attributes 'text' and 'attribute'
ContentLine = namedtuple('ContentLine',
                         'attribute text')  # creates a named tuple class called ContentLine with attributes 'text' and 'attribute'

class ContentDictionary:

    FRAME_NUMBER_REGEX = re.compile('^[0-9]{1,9}[a-z]{1}$')  # reg ex of 1-9 digits with one trailing letter
    PAGE_NUMBER_REGEX = re.compile('^[0-9]{1,9}$')  # reg ex of 1-9 digits with one optional trailing letter

    def __init__(self, base_page_id, max_lines_per_page = 15):

        self.base_page_id = base_page_id
        self._data = []         # list
        self.items = {}         # set/dict

        self._max_lines_per_page = max_lines_per_page               # typically... 24 lines minus head (1), sys (1),
                                                                    # nav (1), title blob (4), blank_lines (2)
        self._available_lines = max_lines_per_page
        self._previous_attribute = ContentAttribute.unknown         # used to keep track of a heading start etc

        # regex patterns
        self._frame_regex = re.compile('^[0-9]{1,9}[a-z]{1}$')      # reg ex of 1-9 digits with one trailing letter
        self._page_regex = re.compile('^[0-9]{1,9}$')               # reg ex of 1-9 digits

    def parse(self, apply_formatting = True):
        """This function creates a dictionary of . The key is the frame_id and the value is a tuple representing the frame content (see below).
        This function is typically used to split content accross multiple dynamic_frames and includes zero routing from frame z.

        :param base_frame_id: accepts a page id (including the frame id) if the frame id element is missing then 'a' is assumed.
        :param apply_formatting: Set this to false to produce a list of unformatted items.
        will be set as ContentAttribute.text.

        :return: returns a dictionary in the following format
                formatted_data = {
                            '123a': (<ContentLine>, <ContentLine>, ...),
                            '123b': (<ContentLine>, <ContentLine>, ...),
                        }
        """
        try:

            current_frame_id = self.tidy_frame_id(self.base_page_id)

            content_items = []
            for unformatted_item in self._data:

                # create the formatted/unformatted lists of content lines and create a content data item
                # this typically represents a single article
                if not apply_formatting:
                    # un-formatted (but comma delimited and trimmed)
                    unformatted_lines = '{0},{1},{2}'.format(unformatted_item[0].strip(), unformatted_item[1].strip(), unformatted_item[2].strip())
                    content_item = ContentDataItem([], [ContentLine(ContentAttribute.text, unformatted_lines.rstrip())], [])

                else:
                    # formatted to fit within the 40 column width
                    content_item = ContentDataItem(ContentFormatter.format(unformatted_item[0], ContentAttribute.heading),
                                                   ContentFormatter.format(unformatted_item[1], ContentAttribute.text),
                                                   ContentFormatter.format(unformatted_item[2], ContentAttribute.date))

                # add the article to the collection of articles
                content_items.append(content_item)

            # we now have all of the lines for the content being processed and need to split them into pages
            # initialise a block with a blank line, this will typically be used to indicate a separator or paragraph
            block = self._init_block()

            for content_item in content_items:

                # we keep track of when headings start so set to a non heading value
                for line in content_item.get_all_lines():
                    h_len = len(content_item.header_lines)

                    # make sure we can fit the header and a couple of lines
                    if (self._is_new_heading(line) and self._available_lines < h_len + 2) or self._available_lines < 1:

                        # no space left so add block to self.items and create a new frame id etc.

                        self.items[current_frame_id] = block
                        block = self._init_block()

                        # check for a continuation
                        if not self._is_new_heading(line) and apply_formatting:
                            # continuation
                            block.append(ContentLine(ContentAttribute.continuation, 'continued...'))
                            self._available_lines -= 1

                        current_frame_id = self.get_follow_on_page_id(current_frame_id)

                    # If a new heading but not a new frame then add a blank
                    # Remember that this is just an empty string with a ContentAttribute.blank
                    # and can be ignored by consumers as appropriate
                    elif self._is_new_heading(line) and not self._is_first_heading(line) and apply_formatting:

                        block.append(ContentLine(ContentAttribute.blank, ''))
                        self._available_lines -= 1

                    block.append(line)

                    # set parameters for next iteration
                    self._available_lines -= 1
                    self._previous_attribute = line.attribute

            # store the final block
            self.items[current_frame_id] = block


        except Exception as ex:
            logger.exception(ex)

    def append(self, heading, text, date):

        # create a tuple for each item added but clean the data first
        self._data.append((self.clean_html(heading),
                           self.clean_html(text),
                           self.clean_html(date))) # append tuple to the list

    def _init_block(self):

        # Remember that this is just an empty string with a ContentAttribute.blank
        # and can be ignored by consumers as appropriate
        # left as empty string so that page generator can do whatever it prefers
        block = [ContentLine(ContentAttribute.blank, '')]
        self._available_lines = self._max_lines_per_page -1
        return block

    def _is_first_heading(self, line):
        return line.attribute == ContentAttribute.heading and self._previous_attribute == ContentAttribute.unknown

    def _is_new_heading(self, line):
        return line.attribute == ContentAttribute.heading and self._previous_attribute != ContentAttribute.heading

    def tidy_frame_id(self, frame_id):

        # check that base page id is valid, ensure that the frame id is present, add as approriate.
        if not self._frame_regex.match(frame_id):

            if not self._page_regex.match(frame_id):
                raise InvalidPageNumberError('The specified page number is invalid.')
            else:
                valid_frame_id = frame_id + 'a'
        else:
            valid_frame_id = frame_id

        return valid_frame_id

    @staticmethod
    def get_follow_on_page_id(current_page_id):

        '''
        Gets the next frame id e.g. 2001a returns 2001b etc. Zero routing is implemented e.g 2001z returns 20010a
        :param current_page_id:
        :return: The next frame ID.
        '''

        # get the frame id i.e. the letter element
        frame_id_asc = ord(current_page_id[-1])
        page_id = current_page_id[:len(current_page_id) - 1]

        # update frame indicator and include zero page routing
        if frame_id_asc < 97:
            raise InvalidFrameIdError('The frame ID is invalid.')
        elif frame_id_asc < 122:
            frame_id_asc += 1
        else:
            frame_id_asc = 97
            page_id += '0'

        return page_id + chr(frame_id_asc)

    @staticmethod
    def bytes_to_string(byte_data):
        return "".join(map(chr, byte_data))

    @staticmethod
    def clean_html(raw_html):

        # Occasionally we get directory references e.g. '.' and '..'
        if raw_html == '..' or raw_html == '.':
            raw_html = ''

        soup = BeautifulSoup(raw_html, features="html.parser")
        cleaned = soup.get_text(separator=u' ')

        # Sort pound and euro unicode chars
        cleaned = cleaned.replace('\u00A3', '\x23').replace('\u20AC', 'EU')
        cleaned = cleaned.replace(' \x7c ', '; ') # the pipe is used a lot in the highways feeds as a separator.
        cleaned = cleaned.replace('Continue reading...', '') # appears at the end of guardian feeds
        cleaned = cleaned.replace('\u2022', '-')
        return cleaned

    # @staticmethod
    # def clean_html(raw_html):
    #
    #     # Occasionally we get directory references e.g. '.' and '..'
    #     if raw_html == '..' or raw_html == '.':
    #         raw_html = ''
    #
    #     soup = BeautifulSoup(raw_html, features="lxml")
    #     cleaned = soup.get_text()
    #
    #     # Sort pound and euro unicode chars
    #     cleaned = cleaned.replace('\u00A3', '\x23').replace('\u20AC', 'EU')
    #
    #     return cleaned

    @staticmethod
    def clean_wiki(raw_wiki):
        """
       Remove wiki markup from the text.
       """
        raw_wiki = re.sub(r'(?i)\{\{IPA(\-[^\|\{\}]+)*?\|([^\|\{\}]+)(\|[^\{\}]+)*?\}\}', lambda m: m.group(2), raw_wiki)
        raw_wiki = re.sub(r'(?i)\{\{Lang(\-[^\|\{\}]+)*?\|([^\|\{\}]+)(\|[^\{\}]+)*?\}\}', lambda m: m.group(2), raw_wiki)
        raw_wiki = re.sub(r'\{\{[^\{\}]+\}\}', '', raw_wiki)
        raw_wiki = re.sub(r'(?m)\{\{[^\{\}]+\}\}', '', raw_wiki)
        raw_wiki = re.sub(r'(?m)\{\|[^\{\}]*?\|\}', '', raw_wiki)
        raw_wiki = re.sub(r'(?i)\[\[Category:[^\[\]]*?\]\]', '', raw_wiki)
        raw_wiki = re.sub(r'(?i)\[\[Image:[^\[\]]*?\]\]', '', raw_wiki)
        raw_wiki = re.sub(r'(?i)\[\[File:[^\[\]]*?\]\]', '', raw_wiki)
        raw_wiki = re.sub(r'\[\[[^\[\]]*?\|([^\[\]]*?)\]\]', lambda m: m.group(1), raw_wiki)
        raw_wiki = re.sub(r'\[\[([^\[\]]+?)\]\]', lambda m: m.group(1), raw_wiki)
        raw_wiki = re.sub(r'\[\[([^\[\]]+?)\]\]', '', raw_wiki)
        raw_wiki = re.sub(r'(?i)File:[^\[\]]*?', '', raw_wiki)
        raw_wiki = re.sub(r'\[[^\[\]]*? ([^\[\]]*?)\]', lambda m: m.group(1), raw_wiki)
        raw_wiki = re.sub(r"''+", '', raw_wiki)
        raw_wiki = re.sub(r'(?m)^\*$', '', raw_wiki)

        return raw_wiki

    @staticmethod
    def printhex(text):
        print(text)
        for c in text:
            print('{0} '.format(hex(ord(c))), end='', flush=True)
        print('')


class ContentDataItem:

    """
    This class typically represents an article with multi-line header, text and date.
    """
    def __init__(self, header_lines, text_lines, date_lines):

        self.header_lines = header_lines
        self.text_lines = text_lines
        self.date_lines = date_lines

    def get_all_lines(self):

        lines = []
        lines.extend(self.header_lines)
        lines.extend(self.text_lines)

        # some feeds have a blank date only add if we have more than one line or exactly one line that has a length > 0
        if len(self.date_lines) > 1 or (len(self.date_lines) > 0 and len(self.date_lines[0].text) > 0):
            lines.extend(self.date_lines)

        return lines


class ContentAttribute(Enum):
    unknown = 0
    text = 1
    heading = 2
    date = 3
    blank = 4
    continuation = 5


class ContentFormatter:

    @staticmethod
    def centre_text(text):
        """
        Centers the text, padding with spaces, within a 40 character line.
        :param text:
        :return:
        """
        l = len(text)
        if l > 0 and l <=40:
            text = ' ' * math.floor((40 - l)/2) + text
        return text

    @staticmethod
    def format(text, content_attribute = ContentAttribute.text, max_line_length = 39):

        """
        This method is designed to format the content into a word wrapped block with each line with no more than <max_line_length> characterss.
        :param text: The text to be formatted.
        :param content_attribute: ContentAttribute describing the type of line e.g. title, standard text etc.
        :param max_line_length: Maximum line length for formatted text.
        :return: A list of lines.
        """

        # list to hold the ContentLine instances
        content_lines = []

        lines = 0
        text_line = ""

        # replace Paras
        text = text.replace("\r\n\r\n", "|#|")  # exchange for a token

        # replace CRs
        text = text.replace("\r", " ").replace("\n", " ") # both exchanged for a space

        # remove multiple spaces replacing with one space
        text = re.sub(' +', ' ', text)

        # Put paras back
        text.replace('|#|', '\r\n\r\n')

        # swap some common combinations
        text = text.replace(' :', ':')
        text = text.replace(' .', '.')
        text = text.replace('  ', ' ') # double spaces removed
        text = text.strip()

        # get the words
        words = text.split(" ");

        # build each line of the formatted text
        for word in words:

            # add the space before counting the length
            word += " "

            if len(text_line) + len (word) <= max_line_length:
                text_line += word
            else:
                # add the line to the formatted text
                content_lines.append(ContentLine(content_attribute, text_line.rstrip()))

                # start a new line with the word that wouldn't fit
                if len(word) <= max_line_length:
                    text_line = word
                else:
                    text_line = "<Word too long>"

                lines += 1

        # add the final word and return
        content_lines.append(ContentLine(content_attribute, text_line.rstrip()))

        return content_lines

if __name__ == '__main__':

    # tests
    cd = ContentDictionary('1234')
    cd.parse()

    frame_content_items = cd.items
    pass
