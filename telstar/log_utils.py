import logging

def getlogger(name: str, level: int = logging.INFO):

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)

    formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
    console_handler.setFormatter(formatter)

    logger.handlers.clear()
    logger.addHandler(console_handler)

    return logger