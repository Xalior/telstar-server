import imp
import os
from telstar.globals import *
import copy
import types
import importlib
import importlib
import sys
import copy
from telstar.event_args import *
from telstar.log_utils import *

logger = getlogger(__name__)

main_module = "__init__"

class PluginLoader:

    def __init__(self,plugin_directory):

        self.plugin_directory = plugin_directory

    def _get_plugins(self):

        plugins = []

        try:
            if not (os.path.exists(self.plugin_directory)):
                logger.warn("The plugin directory ({0}) does not exist.".format(self.plugin_directory))
                return plugins

            possibleplugins = os.listdir(self.plugin_directory)

            for i in possibleplugins:
                location = os.path.join(self.plugin_directory, i)
                if not os.path.isdir(location) or not main_module + ".py" in os.listdir(location):
                    continue
                info = imp.find_module(main_module, [location])
                plugins.append({"name": i, "info": info})
                logger.debug("[plugins] Loaded the '{0}' plugin.".format(i))

            # reverse sorting not absolutely necessary but allows for a reverse
            # priority to be implemented based on plug in name
            plugins = sorted(plugins, reverse=False, key=lambda k: k['name'])

        except Exception as ex:
            logger.exception(ex)

        return plugins

    def _load_module(self, name: str, path: str):

        spec = importlib.util.spec_from_file_location(name, path)
        module = importlib.util.module_from_spec(spec)
        sys.modules[spec.name] = module
        spec.loader.exec_module(module)
        return module

    def run_plugin_with_decorator(self, decorator, *args, **kwargs):

        settings = args[0]
        function_to_run = None

        if decorator == "cron":

            # check for a specific plugin to run
            if len(args) > 1 and type(args[1]) is str:
                function_to_run = args[1]

        functions = []
        for p in self._get_plugins():

            # sort out the name and path
            name = p["name"]

            # could be 1 or more arguments
            #  first argument will be settings
            #  second arg could be event args or function name
            #  third arg could be function name
            if function_to_run is None or function_to_run == name:

                path = self.plugin_directory + name + "/__init__.py"

                # load the module
                module = self._load_module(name, path)

                # look through the modules attributes looking for functions
                # this has to be done before any function is called as the
                # __dict__ is often changed by running a function
                for attribute in module.__dict__.items():
                    if type(attribute[1]) is types.FunctionType:
                        functions.append(attribute[1])

            # now that we have the functions of the module we can check the
            # decorator and run the appropriate ones
            for function in functions  :
                # check that the function has the specified decorator
                if decorator in function.__dict__:
                    # run the function passing the args (note that we use
                    # the '*' unpack operator as args is already a tuple
                    function(*args)
                    functions.clear()

