from enum import Enum
from telstar.log_utils import *

logger = getlogger(__name__)


class FieldType(Enum):
    alpha = 1
    numeric = 2
    alphanumeric = 3


class FrameField:

    def __init__(self, label: str, horz_position, vert_position, required, length = 9, field_type = FieldType.alphanumeric, auto_submit = True):

        self.label = label
        self.vert_pos = vert_position
        self.horz_pos = horz_position
        self.required = required
        self.length = length
        self.field_type = field_type
        self.auto_submit = auto_submit  # when field reaches length, the value is considered to be submitted by the viewdata process
        self.valid = False
        self.value = ''

    # JOHN 15/4/2020 removed as doesn't seem to be needed.
    # def clear(self):
    #     self.value = ''
    #     self.valid = False

