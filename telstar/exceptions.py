import traceback        # keep, as this is often used by importers of exceptions
from telstar.log_utils import *

logger = getlogger(__name__)

class InvalidPageNumberError(Exception):
    """Raise when the videotex page number is invalid."""
class InvalidFrameIdError(Exception):
    """Raise when the videotex frame id is invalid."""
class InvalidFrameContent(Exception):
    """Raise when the videotex frame id is invalid."""
class InvalidFrameType(Exception):
    """Raise when the videotex frame id is invalid."""
class ContentAttributeError(Exception):
    """Raise when the ContentLine attribute is invalid."""
class BlobConcatenationError(Exception):
    """Raised when attempting to concatenating blobs of inappropriate row sizes."""
class AlphagraphicsFormatError(Exception):
    """Raised when formatting of alphagrahics fails."""
class FrameHeaderError(Exception):
    """Raised when trying to assign a bad Header Logo."""
class FrameNotFoundError(Exception):
    """Raised when trying athe specified frame cannot be Found"""
class DocumentDataError(Exception):
    """Raised when there are errors in the Frame Document."""
class ContentTypeError(Exception):
    """Raised when an invalid Content Type has been detected."""
class InvalidFrame(Exception):
    """Raised when a frame is invalid."""
class RedirectError(Exception):
    """Raised when there is a page re-direct error"""
class NotAuthorisedError(Exception):
    """Raised during API calls if the user is not _authenticated."""
class TelstarSessionError(BaseException):
    """Raised by Pad Server when a session error is encountered."""
class PadDirectoryError(BaseException):
    """Raised by Pad Server if supplied call directory is invalid."""
class DatabaseConnectionNotFoundError(BaseException):
    """Raised when a the database connection setting has nor been specified."""
class InvalidCookieSecretError(BaseException):
    """Raised when the specified cookie secret is invalid."""
class InvalidApiPasswordError(BaseException):
    """Raised when creating a user and the specified password invalid e.g. too short."""
class SettingNotFounrError(BaseException):
    """Raised when the specified setting is not present."""
