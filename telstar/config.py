import json
from telstar.globals import *
from telstar.exceptions import *
from telstar.log_utils import *

logger = getlogger(__name__)


class Config:

    def __init__(self, path_to_user_settings):

        # get user settings
        f = open(path_to_user_settings)
        json_data = f.read()
        self.settings = json.loads(json_data)
        f.close()

        # add some defaults for any that are missing
        for key, value in k_default_settings.items():
            if key not in self.settings:
                self.settings[key] = value

    @staticmethod
    def get_setting(settings: dict, key: str):

        if key in settings.keys():
            value = settings[key]
        else:
            raise SettingNotFounrError()

        return value






